###############################################################################
#
# Authors:      Jessica Nephin
# Affiliation:  Fisheries and Oceans Canada (DFO)
# Group:        Marine Spatial Ecology and Analysis
# Location:     Institute of Ocean Sciences
# Contact:      e-mail: jessica.nephin@dfo-mpo.gc.ca | tel: 250.363.6564
#
# Overview:
# Evaluates Dungeness crab models with CV data, facvars and spVars set to NULL
# Uses raster predictions for eval like for HSI model predictions
#
###############################################################################


# Move back to parent directory
# Note: never run this twice in one session
setwd('..');setwd('..');setwd('..')



########################
##### Housekeeping #####
########################


# General options
rm( list=ls( ) )           # Clear the workspace
options(scipen=999)        # Remove scientific notation
options("rgdal_show_exportToProj4_warnings"="none")  # Silence proj warnings

# source functions
source("Scripts/DataPrep-Functions.R")
source("Scripts/SDM-Functions.R")

# Load required packages
UsePackages( c("parallel", "raster", "sp", "rgdal", "dismo", "rgeos",
               "ggplot2", "gridExtra", "dplyr", "reshape2", "classInt",
               "ModelMetrics", "PresenceAbsence", "purrr",
               "colorRamps", "viridis", "RColorBrewer", "blockCV") )


# Allow for command line arguments
args = commandArgs(trailingOnly=TRUE)


########################
#####   Controls   #####
########################

# Project
projDir <- "Dungy"

# Comparison folders
comp <- "Combined_gear"

# Name of species (must match directory name)
spp <- "XKG"

# Eval type
modtype <- "CV_Raster"

# Threshold selection method 
# for converting probability predictions to binary (presence/absence) 
# for accepted option see: ?PresenceAbsence::optimal.thresholds
statPA <- "MaxSens+Spec"

# Breaks method for converting prediction into high/med/low categories
# See ?classIntervals for break style options
breakstyle <- "kmeans"

# Multiplier to use to determine resolution for aggregated residual plot
# E.g., env raster resolution = 100m, aggregateBy = 50, aggregated raster = 5 km
aggregateBy <- 50

# Position of legend in exported maps
# Options include "bottomleft", "bottomright", "topleft", "topright"
legendPos = "bottomleft"

# Image file format for figures (pdf or png)
ff <- "png"





######################
#####  Start log  ####
######################

# Sink output to file
rout <- file( file.path(outdir, paste0(modtype, "_eval.log")), open="wt" )
sink( rout, split = TRUE ) # display output on console and send to log file
sink( rout, type = "message" ) # send warnings to log file
options(warn=1) # print warnings as they occur

# Start the timer
sTime <- Sys.time( )





#######################
####  Load Inputs  ####
#######################


# path to Inputs
indir <- file.path("Projects", projDir, "Comparisons", comp, "DataPrep/Data")


# Load land polygon
load( file=file.path(indir, "LandPolygon.RData") )
# Load facVars and facLevels
load( file=file.path(indir, "facVars.RData") )




#------------------------------------------------------------------------------#
# Run once for each facLevel

for (l in facLevels){
  
  
  # type 
  outType <- paste0("CV_Raster_",l)
  

  
  #######################
  ####  Directories  ####
  #######################
  
  # Subfolders for output (i.e., ./spp/outData/)
  outdir <- file.path("Projects", projDir, "Comparisons", comp, "Model", "GAM")
  outData <- "Data"
  outFigs <- "Figures"

  # Set up directories
  dir.create( file.path(outdir, spp, outType), recursive = T )
  dir.create( file.path(outdir, spp, outType, outData) )
  dir.create( file.path(outdir, spp, outType, outFigs) )

  
  #######################
  ####  Load Inputs  ####
  #######################
  
  indir <- file.path("Projects", projDir, "Comparisons", l, "DataPrep/Data")
  
  # Load cv blocks
  load( file=file.path(indir, "cvBlocks.RData") )
  # Load environmental layers
  load( file=file.path(indir, "EnviroLayers.RData") )
  # Load species and environmental data
  load( file=file.path(indir, "Species-EnviroDat.RData") )
  
  
  # Number of CV folds
  numFolds <- length(cv.list[[spp]]$cv)
  
  
  # Load predictions
  pred.list <- list()
  for( f in 1:numFolds){
    pred_filepath <- file.path("Projects", projDir, "Comparisons", comp, "Model", 
                               "GAM", spp, "Env/Data", 
                               paste0("Prediction_fold", f, "_", l, ".tif"))
    pred.list[[paste0("fold",f)]] <- pred_filepath
  }
  predstack <- stack(pred.list)
  
  
  
  
  #########################
  #####   Prep Data   #####
  #########################
  
  # Select species data
  obs <- alldat$sp$sppDat[ ,spp, drop = FALSE]
  # Create spatial points data frame for obs
  spdf <- SpatialPointsDataFrame( coords=alldat$sp$spatDat,
                                  data=cbind(obs),
                                  proj4string=CRS(proj4string(alldat$env)) )
  
  
  
  ###############################
  ####  Start Summary Table  ####
  ###############################
  
  # Statistics
  summarystats <- c("TjurR2", "AUC", "Threshold", "Kappa",
                    "TSS", "Accuracy", "Sensitivity", "Specificity")
  
  # Start summary table to fill with results
  spnames <- spp
  spTable <- data.frame( Species=spnames, stringsAsFactors = FALSE,
                         ModelType = outType, Data= "Test", 
                         Stat="", Mean="", SD="")
  spTable <- spTable[rep(seq_len(nrow(spTable)), each=length(summarystats)),]
  spTable$Stat <- summarystats
  
  
  
  
  
  ##############################################
  #####  Extract predictions and evaluate  #####
  ##############################################
  
  # Extract predicted values at location of test observations
  # Calculate threshold-independent statistics using  test data
  # Also returns mean test obs and predictions
  evaldat <- evalStats( folds=1:numFolds,
                        spdf=spdf, 
                        CV=cv.list[[spp]]$cv,
                        spTable=spTable,
                        preds=predstack)   
  
  
  # Calculate threshold-dependent statistics using test data
  threshdat <- evalPAStats( folds=1:numFolds,
                            traindat=NA,
                            testdat=evaldat[["test.dat"]],
                            stat=statPA,
                            spTable=spTable)
  
  # Calculate mean residual values
  # Observations minus mean predictions
  resids <- spdf@data[[1]] - evaldat$test.meanpreds
  
  
  
  
  ##########################
  ####  Summary Output  ####
  ##########################
  
  # Write summary table to csv
  write.csv( x=spTable, row.names=FALSE, na="",
             file=file.path( outdir, paste0(outType, "_summaryTable.csv")) )
  
  
  # Write evaluation stats from each fold 
  write.csv( round( data.frame( fold=1:numFolds, 
                                evaldat$allstats, 
                                threshdat$allstats), 2),
             file=file.path( outdir, spp, outType, outData, 
                             "stats-allfolds.csv"), row.names=FALSE )
  
  
  
  ###################
  ##### Figures #####
  ###################
  
  # Plot observed versus predicted (PA) categories
  PlotObsPredClass( obs=spdf@data[[1]], 
                    preds=evaldat[["test.meanpreds"]], 
                    thresh=threshdat$mean.stats[["test.Threshold"]], 
                    type="test", 
                    height=4, width=6 )
  
  # Plot histogram of prediction
  PlotHist( vals=getValues(predstack[[1]]),
            type = "Predictions",
            height=4, width=6)
  
  #Plot Accuracy over threshold ranges
  PlotAccuracy( dat=threshdat[["test.statRange"]],
                opt=threshdat$mean.stats[["test.Threshold"]],
                type="test",
                height=4, width=6 )
  
  # Plot receiver operating characteristic curve
  PlotROC( dat=threshdat[["test.statRange"]],
           AUC=evaldat$mean.stats[["test.AUC"]],
           opt=threshdat$mean.stats[["test.Threshold"]],
           type="test",
           height=4.5, width=4.5 )
  
}


######################
#####   End log   ####
######################

# Print end of file message and elapsed time
cat( "\nFinished: ", sep="" )
print( Sys.time( ) - sTime )

# Stop sinking output
sink( type = "message" )
sink( )
closeAllConnections()

