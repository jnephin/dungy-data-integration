# packages
require(ggplot2)
require(dplyr)
require(reshape2)
require(gridExtra)
require(raster)
require(GGally)
require(scales)


#------------------------------------------------------------------------------#
# Create dir
dir.create("Analysis/Figures", recursive = T)

# projects
projects <- c("Dive", "Trawl", "Trap", "Combined", "Combined_gear",
              "Ensemble", "Ensemble_integrated")
# model types 
types <- c("GAM", "CV_Raster", "CV_Raster_Dive", "CV_Raster_Trawl", 
           "CV_Raster_Trap", "CV_Raster_Combined", "Holdout")


# Species
s <- "XKG"



#------------------------------------------------------------------------------#
# Get stats

# empty objects to fill
stats <- NULL

# loop through projects and types for evaluation stats
for (p in projects){
  # locations
  loc <- file.path(p, "Model", "GAM")
  # for all types
  for (j in types){
    f <- file.path(loc, paste0(j, "_summaryTable.csv") )
    if (file.exists(f)){
      stable <- read.csv(f)
      # read summary table
      stable$Species <- p
      names(stable)[names(stable) == "Species"] <- "model"
      names(stable)[names(stable) == "ModelType"] <- "type"
      # Add to summary objects
      stats <- rbind( stats, stable)
    }
  }
}


# Subset
stats <- stats[stats$Stat %in% c('AUC', 'TjurR2'),]
stats <- stats[stats$type != "Env-SAC",]


# Eval data
stats$eval <- stats$type
stats$eval[stats$type %in% c("CV_Raster","Env")] <- 
  stats$model[stats$type %in% c("CV_Raster","Env")]
#stats$eval[stats$type == "Holdout"] <- "Trap"
stats$eval <- sub( "CV_Raster_", "", stats$eval )
stats$eval <- sub( "_gear", "", stats$eval )

# Rename 
stats$type[grepl("CV_Raster", stats$type)] <- "CV"
stats$model[stats$model == "Ensemble"] <- "Ensemble_single"

# Factor levels
stats$Data <- factor(stats$Data, levels = c("Train","Test"))
stats$model <- sub("_","\n",stats$model)
stats$model <- factor(stats$model, 
                    levels = rev(c("Dive", "Trawl", "Trap", "Combined", "Combined\ngear",
                                   "Ensemble\nsingle", "Ensemble\nintegrated")))


#--------------------------------------------------------------------------#
# Comparison figures


# Difference between train and test
tmp <- stats[stats$type  == "Env",]
tmp$Min <- 0
tmp$Min[tmp$Stat == "AUC"] <- 0.5
tmp$Lower <- tmp$Mean-tmp$SD
tmp$Lower[tmp$Lower < 0] <- 0
gPlot <- ggplot( data=tmp, 
                  aes(x=model, group=Data) ) +
  geom_linerange( aes( ymin=Min, ymax=Mean, colour=Data), size=6,
                  position=position_dodge2(width=.9, preserve = "single", reverse=TRUE) ) +
  geom_linerange( aes( ymin=Lower, ymax=Mean+SD), size=.4, colour="black",
                  position=position_dodge2(width=.9, preserve = "single", reverse=TRUE) ) +
  facet_grid(.~Stat, scales="free") +
  scale_colour_manual( values = c("grey80","grey50")) +
  coord_flip( ) +
  scale_y_continuous(expand = expansion(mult = c(0, .03)))+
  theme_bw( ) +
  theme( axis.title.y = element_blank(),
         strip.background = element_blank(),
         legend.key=element_blank(), 
         legend.background=element_blank(), 
         legend.title=element_blank(),
         axis.title=element_blank(),
         legend.position="bottom",
         panel.border=element_rect(colour="black"),
         panel.grid = element_blank() )+
  guides(colour = guide_legend(override.aes = list(size = 6)))
gPlot
# Save the plot
ggsave( plot=gPlot, height=3.4, width=7, 
        filename=paste0("Analysis/Figures/","Eval_CV_Test-Train.png"))



# Difference between evaluation data types
tmp <- stats[stats$type != "Env",]
ma <- tmp %>% group_by(model,Stat) %>%
  summarise( Mean=mean(Mean),
             SD=0 ) %>% as.data.frame()
ma$eval <- "Mean"
tmp <- rbind(tmp[names(ma)],ma)
tmp$Min <- 0
tmp$Min[tmp$Stat == "AUC"] <- 0.45
tmp$Lower <- tmp$Mean-tmp$SD
tmp$Lower[tmp$Stat == "AUC" & tmp$Lower < 0.45] <- 0.45
tmp$Lower[tmp$Stat == "TjurR2" & tmp$Lower < 0] <- 0
tmp$eval <- factor(tmp$eval, levels=c("Dive","Trawl","Trap", "Combined", "Holdout","Mean"))

# summary stats
ma
mAUC <- max(ma[ma$Stat == "AUC", "Mean"])
mtjur <- max(ma[ma$Stat == "TjurR2", "Mean"])
ma[ma$Mean %in% c(mAUC,mtjur),]

tAUC <- max(tmp[tmp$Stat == "AUC", "Mean"])
ttjur <- max(tmp[tmp$Stat == "TjurR2", "Mean"])
tmp[tmp$Mean %in% c(tAUC,ttjur),]

gPlot  <- ggplot( data=tmp, 
                  aes(x=model, group=eval) ) +
  geom_linerange( aes( ymin=Min, ymax=Mean, colour=eval), size=3.2,
                  position=position_dodge2(width=.9, preserve = "single", reverse=TRUE) ) +
  geom_linerange( aes( ymin=Lower, ymax=Mean+SD), size=.4, colour="black",
                  position=position_dodge2(width=.9, preserve = "single", reverse=TRUE) ) +
  facet_grid(.~Stat, scales="free") +
  scale_colour_manual( values = c("#E69F00","#56B4E9","#009E73","#F0E442","#0072B2","grey") ) +
  coord_flip( ) +
  scale_y_continuous(expand = expansion(mult = c(0, .03)))+
  theme_bw( ) +
  theme( axis.title.y = element_blank(),
         strip.background = element_blank(),
         legend.key=element_blank(), 
         legend.background=element_blank(), 
         legend.title=element_blank(),
         axis.title=element_blank(),
         legend.position="bottom",
         panel.border=element_rect(colour="black"),
         panel.grid = element_blank() ) +
  guides(colour = guide_legend(override.aes = list(size = 5)))
gPlot
# Save the plot
ggsave( plot=gPlot, height=6.2, width=7.5, 
        filename=paste0("Analysis/Figures/","EvalTypes_Test.png"))






#----------------------------------------------------------------------------------#
# Get preds, rel inf and margs

relinf <- NULL
marg <- NULL
margRanges <- list()
preds <- list()
sdvals <- list()
deciles <- list()
obsdat <- NULL
envdat <- NULL

# loop through projects for predictions, relinf and marginal effects
for (p in projects){
  # locations
  loc <- file.path(p, "Model", "GAM")
  if ( !grepl("Ensemble", p) ){
    # read relimp table
    prel <- read.csv( file.path(loc, s, "Env/Data",  "predictor-relimp.csv") )
    prel$model <- p
    relinf <- rbind( relinf, prel)
    # get mean margs and obs from workspace image
    load( file.path(loc, s, "Env/Env_Workspace.RData"))
    marg_mean <- margs[["mean"]]
    marg_mean$model <- p
    marg <- rbind( marg, marg_mean)
    margRanges[[p]] <- margs[["ranges"]]
    # get obs from workspace
    obsvals <- alldat$sp$sppDat
    obsvals$model <- p
    obsdat <- rbind( obsdat, obsvals)
    # get obs env dataset
    envvals <- alldat$env@data
    envvals <- envvals[!names(envvals) %in% 
                         c('Dive_Prediction', 'SmallMesh_Prediction','Gear', 'Survey')]
    envvals$model <- p
    envdat <- rbind( envdat, envvals)
  }
  # get raster pred, decile and sd values
  if ( !grepl("Ensemble", p) && p != "Combined_gear" ){ 
    r <- raster( file.path(loc, s, "Env/Data", "Predicted_Mean.tif") )
    sds <- raster( file.path(loc, s, "Env/Data", "Predicted_SD.tif") )
    # d <- raster( file.path(loc, s, "Env/Data", "Decile.tif") )
    ptmp <-  getValues(r)
    preds[[p]] <-  ptmp[!is.na(ptmp)]
    } else if ( p == "Combined_gear" ){
    for (j in c("Dive", "Trawl", "Trap")){
      pname <- paste0("Combined_", j) 
      r <- raster( file.path(loc, s, "Env/Data", paste0("Predicted_Mean_", j, ".tif")) )
      # d <- raster( file.path(loc, s, "Env/Data", "Decile_Trap.tif") )      
      ptmp <-  getValues(r)
      preds[[pname]] <-  ptmp[!is.na(ptmp)]
      }
    sds <- raster( file.path(loc, s, "Env/Data", "Predicted_SD_Trawl.tif") )
  } else {
    r <- raster( file.path(loc, s, "Holdout/Data", "Ensemble_Mean.tif") )
    sds <- raster( file.path(loc, s, "Holdout/Data", "CV_SD.tif") )
    # d <- raster( file.path(loc, s, "Holdout/Data", "Decile.tif") )
    ptmp <-  getValues(r)
    preds[[p]] <-  ptmp[!is.na(ptmp)]
  }
  sdtmp <- getValues(sds)
  sdvals[[p]] <- sdtmp[!is.na(sdtmp)]
}



# Add holdout data to obs
load( file.path('Dive', "Model", "GAM", s, "Holdout/Holdout_Workspace.RData"))
obsvals <- alldat$sp$sppDat
obsvals$model <- "Holdout"
obsdat <- rbind( obsdat, obsvals)
obsdat <- obsdat[obsdat$model %in% 
                   c("Dive", "Trawl", "Trap", "Combined", "Holdout"),]
obsdat$model <- factor(obsdat$model, 
                       levels=rev(c("Dive", "Trawl", "Trap", 
                                    "Combined", "Holdout")))




#--------------------------------------------------------------------------#
# obs

obsdat$count <- 0
obsagg <- aggregate(count ~ model + XKG, FUN=length, data=obsdat)
obsagg <- obsagg %>% group_by(model) %>% 
  mutate(total=sum(count)) %>% as.data.frame()
obsagg$percent <- paste0(as.character(
  round(100 * obsagg$count / obsagg$total)), " %")
obsagg$percent[obsagg$XKG == 0] <- ""

# Difference between datasets
gPlot  <- ggplot( data=obsagg, aes(x=model,y=count, fill=factor(XKG)) ) +
  geom_bar(width=.8, stat="identity") + 
  geom_text(aes(label=percent), size=3, hjust=-0.1) +
  scale_fill_manual( name="Detection", values = c("grey80","grey50")) +
  coord_flip( ) +
  scale_y_continuous(expand = expansion(mult = c(0, .02)))+
  xlab( "" ) +
  ylab( "Number of observations" ) +
  theme_bw( ) +
  theme( axis.title.y = element_blank(),
         legend.key=element_blank(), 
         legend.position=c(.8,.8),
         panel.border=element_rect(colour="black"),
         panel.grid = element_blank() )+
  guides(colour = guide_legend(override.aes = list(size = 6)))
gPlot
# Save the plot
ggsave( plot=gPlot, height=3.5, width=6, 
        filename=paste0("Analysis/Figures/","Obs_Datasets.png"))




#--------------------------------------------------------------------------#
# envdat

envdat <- envdat[envdat$model %in% c("Dive", "Trawl", "Trap"),]

# Get predictors (in order of rel imp)
pred_names <- names(envdat)[-length(names(envdat))]
num <- length(pred_names)

# levels
envdat$model <- factor(envdat$model, levels=c("Dive", "Trawl", "Trap") )


# Plot list
gpList <- list( )

# Loop over predictors
for( i in pred_names ) {
  # Grab the data
  ienv <- envdat[names(envdat) %in% c(i, 'model')]
  colnames(ienv) <- c("Value", "model")
  # plot
  gpList[[i]] <- ggplot( data=ienv, 
                         aes(x=Value, colour=model) ) +
    theme_bw( ) +
    scale_y_continuous( expand = expansion(mult = c(0, .1)) ) +
    scale_color_manual( name="",
                       values = c("#E69F00","#56B4E9","#009E73")) +
    theme(panel.grid = element_blank(),
          legend.text = element_text(size=12),
          axis.title.y = element_blank(),
          axis.text.y = element_blank(),
          axis.ticks.y = element_blank(),
          plot.margin = margin(2, 2, 2, 2, "pt")) +
    guides(colour = guide_legend(override.aes = list(size = 1.2)))
  
  # plot density
  gpList[[i]] <-  gpList[[i]] +
    geom_density( fill=NA, show.legend=FALSE) +
    stat_density(geom="line",position="identity", size=.7) +
    labs( x=i, y="Relative density" )
}


# Get legend from last grob
lgrobs <- ggplot_gtable(ggplot_build(gpList[[length(gpList)]]))
guide_box <- which(sapply(lgrobs$grobs, function(x) x$name) == "guide-box")
# Remove individual legends
for (i in seq_len(length(gpList)) ){
  gpList[[i]] <- gpList[[i]] + theme(legend.position="none")
}
# Add legend grob
gpList[[length(gpList)+1]] <- lgrobs$grobs[[guide_box]]

# Arrange the plots
gridPlots <- do.call( what=arrangeGrob, args=c(grobs=gpList, ncol=3) )

# Save the plot
ggsave( plot=gridPlots, height=7.5, width=6.5, 
        filename=paste0("Analysis/Figures/","Env_Datasets.png"))



#--------------------------------------------------------------------------#
# SD

# mean
sdMean <- lapply(sdvals, FUN=mean) 
sdSD <- lapply(sdvals, FUN=sd) 
sdagg <- data.frame(mean=unlist(sdMean), sd=unlist(sdSD))
sdagg$model <- row.names(sdagg)


# levels
sdagg$model[sdagg$model == "Ensemble"] <- "Ensemble_single"
sdagg$model <- factor(sdagg$model, 
                      levels=rev(c("Dive", "Trawl", "Trap", "Combined", "Combined_gear",
                                   "Ensemble_single", "Ensemble_integrated")))

# Difference between datasets
gPlot  <- ggplot( data=sdagg, aes(x=model,y=mean) ) +
  geom_bar(width=.8, stat="identity", fill="grey50") + 
  geom_linerange( aes( ymin=mean-sd, ymax=mean+sd), size=.4, colour="black" ) +
  coord_flip( ylim = c(0,.3) ) +
  scale_y_continuous(expand = expansion(mult = c(0, .02))) +
  xlab( "" ) +
  ylab( "Standard deviation" ) +
  theme_bw( ) +
  theme( axis.title.y = element_blank(),
         panel.border=element_rect(colour="black"),
         panel.grid = element_blank() )
gPlot
# Save the plot
ggsave( plot=gPlot, height=3.5, width=6, 
        filename=paste0("Analysis/Figures/","Prediction_SD.png"))




#--------------------------------------------------------------------------#
# Rel imp

# Aggregate
relagg <- relinf %>% group_by(terms, model) %>%
  summarise(Mean = mean(relimp), SD = sd(relimp)) %>% as.data.frame()
relagg <- relagg %>% group_by(terms) %>%
  mutate(Sum = sum(Mean)) %>% arrange(desc(Sum)) %>% as.data.frame()


# Ensure proper variable order
relagg$terms <- factor( relagg$terms, 
                        levels=unique(relagg$terms[order(relagg$Sum)]) )
relagg$model <- factor(relagg$model, levels = projects)



# Mean  rel imp for Env models
gPlot  <- ggplot( data=relagg, 
                  aes(x=terms, y=Mean, ymin=Mean-SD, ymax=Mean+SD, fill=model) ) +
  geom_col( position=position_stack(reverse=TRUE), width=.8 ) +
  scale_fill_manual( values = c("#E69F00","#56B4E9","#009E73","#F0E442","#0072B2")) +
  coord_flip() +
  scale_y_continuous(expand = expansion(mult = c(0, .02)))+
  xlab( " " ) +
  ylab( "Relative influence" ) +
  theme_bw( ) +
  theme( legend.key=element_blank(), 
         legend.title=element_blank(),
         legend.position=c(.82,.35),
         panel.border=element_rect(colour="black"),
         panel.grid = element_blank() )
gPlot
# Save the plot
ggsave( plot=gPlot, filename=paste0("Analysis/Figures/","Mean_RelImp.png"), 
        height=3.5, width=7 )





#--------------------------------------------------------------------------#
# Marginal effects

# Subset
marg <- marg[marg$model %in% 
               c("Dive", "Trawl", "Trap", "Combined", "Combined_gear"),]

# Add columns for figure
marg$range <- 'inside'
marg$line <- 'solid'
prednames <- unique(marg$Predictor)

# Ensure proper variable order
marg$model <- factor(marg$model, 
                     levels = c("Dive", "Trawl", "Trap", "Combined", "Combined_gear"))


# Get predictors (in order of rel imp)
pred_names <- unique(as.character(relagg$terms[order(relagg$Sum, decreasing = T)]))
num <- length(pred_names)

# Number of columns
numCols <- 3
c1 <- seq(1, length(pred_names)+1, by = numCols)
c2 <- seq(2, length(pred_names)+1, by = numCols)
c3 <- seq(3, length(pred_names)+1, by = numCols)
axis_preds <- pred_names[c1]


# Plot marginal effect of most important predictors
pltList <- list( )
# Loop over predictors
for( i in pred_names ) {
  # Grab the data
  iMarg <- marg[marg$Predictor == i,]
  
  # Convert obs to numeric if not factor
  if( i != "Gear" ){
    iMarg$obs <- as.numeric( iMarg$obs )
    for (p in c("Dive", "Trawl", "Trap", "Combined", "Combined_gear") ){
      iRange <-  margRanges[[p]][i]
      iMarg$range[iMarg$model == p & iMarg$obs < iRange[1,]] <- 'lesser'
      iMarg$range[iMarg$model == p & iMarg$obs > iRange[2,]] <- 'greater'
      iMarg$line[iMarg$model == p & iMarg$obs < iRange[1,]] <- '12'
      iMarg$line[iMarg$model == p & iMarg$obs > iRange[2,]] <- '12'
    }
    # add group
    iMarg$grp <- paste0(iMarg$model, iMarg$range)
  }
  # Breaks
  if( i != "Gear"){
    b <- seq(min(iMarg$obs), max(iMarg$obs), length.out=6)
    b <- b[-1] 
    b <- b[-length(b)] 
  }
  # The plot
  pltList[[i]] <- ggplot( data=iMarg ) +
    xlab( i ) +
    ylab( "f(x)" ) +
    scale_linetype_identity( ) +
    scale_y_continuous( limits=c(0,1.02), expand=c(0,0) ) +
    theme_bw( ) +
    theme( panel.border=element_rect(colour="black"),
           panel.grid = element_blank(),
           legend.text = element_text(size=11),
           legend.background =  element_blank(),
           plot.margin = margin(5, 10, 2, 2, "pt"))
  # For gear
  if( i == "Gear" ) {
    pltList[[i]] <- pltList[[i]] +
      geom_bar( data=iMarg, aes(x=obs, y=mean), 
                fill="#0072B2", stat = "identity" )
  } else {
    pltList[[i]] <- pltList[[i]] +
      geom_line( data=iMarg, lwd= .9,
               aes(x=obs, y=mean, group=grp, linetype=line, colour=model) ) +
      scale_colour_manual( name="", 
                           values = c("#E69F00","#56B4E9","#009E73","#F0E442","#0072B2"))
  }
    
  if( i %in% c("sandy","muddy")){
    pltList[[i]] <- pltList[[i]] + scale_x_continuous( expand=c(0,0), breaks=b )
  } else if (i != "Gear") {
    pltList[[i]] <- pltList[[i]] + scale_x_continuous( expand=c(0,0), n.breaks=4 )
  }
  if ( !i %in% axis_preds){
    pltList[[i]] <- pltList[[i]]  +
      theme( axis.title.y = element_blank(),
             axis.text.y = element_blank() )
  }
}  # End i loop over predictors


# Get legend from last grob
lgrobs <- ggplot_gtable(ggplot_build(pltList[[length(pltList)]]))
guide_box <- which(sapply(lgrobs$grobs, function(x) x$name) == "guide-box")
# Remove individual legends
for (i in seq_len(length(pltList)) ){
  pltList[[i]] <- pltList[[i]] + theme(legend.position="none")
}
# Add legend grob
pltList[[length(pred_names)+1]] <- lgrobs$grobs[[guide_box]]


# Arrange the plots
col1 <- do.call( what=arrangeGrob, 
                 args=c(grobs=pltList[c1], ncol=1))
col2 <- do.call( what=arrangeGrob, 
                 args=c(grobs=pltList[c2], ncol=1))
col3 <- do.call( what=arrangeGrob, 
                 args=c(grobs=pltList[c3], ncol=1))
gridPlots <- grid.arrange(col1,col2,col3, ncol=numCols, 
                          widths=c(5,4.1,4.1))
  
# Save
ggsave( plot=gridPlots, 
        height=ceiling(num/numCols)*1.8,
        width=numCols*2.7,
        filename="Analysis/Figures/MeanMarginalEffects.png")




#--------------------------------------------------------------------------#
# Compare prediction values

# Remove index if any preds are NA
str(preds)
dat <- as.data.frame(preds)
# isna <- apply(dat, 1, FUN=function(x) any(is.na(x)) )
# dat <- dat[!isna,]

# add names
colnames(dat) <-  c("Dive", "Trawl", "Trap", "Combined", 
                    "Combined_Dive","Combined_Trawl", "Combined_Trap",
                    "Ensemble_single", "Ensemble_integrated")
colnames(dat) <- sub("_","\n",colnames(dat))

# Remove
#datsub <- dat[,-(1:3)]

# Thin the data if there's lots (5%)
thin_preds <- sample_n( tbl=dat, size=nrow(dat)/10 )


# size
pltsize=7.5


# smooth for ggpairs
my_smooth <- function(data, mapping, ...){
  ggplot(data = data, mapping = mapping, ...) + 
    do.call(geom_point, list(pch=16, size=1, alpha=.05, colour="grey20") ) +
    #do.call(geom_smooth, list(method="lm", formula='y~x', colour="#0072B2")) +
    do.call(lims, list(y=c(0,1),x=c(0,1))) 
}

# blank for ggpairs
my_blank <- function(data, mapping, ...){
  ggplot(data = data, mapping = mapping, ...) + 
    do.call(geom_point, list(size=0, colour="white") ) 
}

# Pairs plots
gPlot <- GGally::ggpairs( 
  data=thin_preds, axisLabels="show", progress = FALSE,
  lower=list(continuous=GGally::wrap(my_smooth),
             combo = wrap("box_no_facet",colour = "black", fill = "grey80", 
                          outlier.size = 1, outlier.shape = 16)),
  upper=list(continuous = GGally::wrap("cor", size=pltsize/2, stars=FALSE, 
                                       digits=2, title="Cor"),
             combo = wrap(my_blank)),
  diag=list(continuous = GGally::wrap("densityDiag", fill='grey80')) )
# Add theme
gPlot <- gPlot + theme_bw( ) +
  theme( strip.background = element_blank(),
         axis.text = element_blank(),
         strip.text.x = element_text(size=pltsize*1.2, angle = 90, hjust=0),
         strip.text.y = element_text(size=pltsize*1.2, angle = 0, hjust=0),
         panel.grid = element_blank())
# Save the plot
ggsave( plot=gPlot, height=pltsize, width=pltsize,
        filename="Analysis/Figures/Prediction_Pairs.png")




# Density plot
dp <- melt(dat)


gPlot <- ggplot( data=dp, aes(x=value, colour=variable) ) +
  geom_density( fill=NA, show.legend=FALSE) +
  stat_density(geom="line",position="identity", size=.7) +
  theme_bw( ) +
  theme(panel.grid = element_blank(),
        legend.text = element_text(size=12),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank(),
        plot.margin = margin(2, 2, 2, 2, "pt")) +
  guides(colour = guide_legend(override.aes = list(size = 1.2)))
gPlot



