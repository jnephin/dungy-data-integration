#!/c/cygwin64/bin/bash
# Run in command line from Scripts directory
# bash Run-Dungy.sh
# Run DataPrep.R for each dungy dataset
# -----------------------------------------------------#


# # loop through datasets
#  Trap_covariates
for type in Trawl Dive Trap Combined Combined_gear
do
  # Dive
  if [ "$type" == "Dive" ]
  then
    #shapefile
    shpfle="DiveBHM_XKG_Lines_2013-2019.shp"
    #facVars
    facvars="NULL"
  fi
  # small mesh trawl
  if [ "$type" == "Trawl" ]
  then
    #shapefile
    shpfle="SmallMesh_GFBio_XKG_Lines_2000-2019.shp"
    #facVars
    facvars="NULL"
  fi
  # Crabbio
  if [ "$type" == "Trap" ]
  then
    #shapefile
    shpfle="Crabbio_all_XKG_Lines_2005-2016.shp"
    #facVars
    facvars="NULL"
  fi
  # Crabbio with survey factor
  if [ "$type" == "Trap_survey" ]
  then
    #shapefile
    shpfle="Crabbio_all_XKG_Lines_2005-2016.shp"
    #facVars
    facvars="Survey"
  fi
  # All 3 sources with gear factor
  if [ "$type" == "Combined" ]
  then
    #shapefile
    shpfle="AllSources_XKG_Lines_2000-2019.shp"
    #facVars
    facvars="NULL"
  fi
  # all 3 sources with gear factor
  if [ "$type" == "Combined_gear" ]
  then
    #shapefile
    shpfle="AllSources_XKG_Lines_2000-2019.shp"
    #facVars
    facvars="Gear"
  fi
  # Crabbio, predictions from dive and smallmesh
  if [ "$type" == "Trap_covariates" ]
  then
    cd ..
    covdir="Projects/Dungy/Data/EnvData/covariates"
    cd $covdir
    mv * ../
    cd ../../../../../
    cd Scripts
    #shapefile
    shpfle="Crabbio_all_XKG_Lines_2005-2016.shp"
    #facVars
    facvars="NULL"
  fi

  # Run Data prep
  Rscript --vanilla --verbose DataPrep.R $shpfle $facvars
  # Run model
  Rscript --vanilla --verbose SDM.R

  # Create new dataset folder
  cd ..
  prepdir="Projects/Dungy/Comparisons/$type/"
  moddir="Projects/Dungy/Comparisons/$type/Model/"
  mkdir -p "$prepdir"
  mkdir -p "$moddir"

  # Move DataPrep and Models to new folder
  mv "Projects/Dungy/DataPrep/" "$prepdir"
  mv "Projects/Dungy/Models/GAM/" "$moddir"
  # Move back to scripts folder
  cd Scripts
done

## Run Eval
cd ..
cd Projects/Dungy/Comparisons


# Run Combined gear type cv-raster
Rscript --vanilla --verbose Evaluate-Dungy-CV-Combined_gear.R # dive, trawl, trap
Rscript --vanilla --verbose Evaluate-Dungy-Combined_gear.R # combined


# All other integrated CV-raster evals
for type in Trawl Dive Trap Ensemble Ensemble_integrated Combined
do
    # dive
    Rscript --vanilla --verbose Evaluate-Dungy-CV-Raster.R $type Dive
    # trawl
    Rscript --vanilla --verbose Evaluate-Dungy-CV-Raster.R $type Trawl
    # trap
    Rscript --vanilla --verbose Evaluate-Dungy-CV-Raster.R $type Trap
    # combined
    Rscript --vanilla --verbose Evaluate-Dungy-CV-Raster.R $type Combined
done

# All single source CV-raster evals and combined
for type in Trawl Dive Trap
do
    # Run eval
    Rscript --vanilla --verbose Evaluate-Dungy-CV-Raster.R $type $type
done



# Holdout eval (ensemble happens in ensemble script)
for type in Trawl Dive Trap Combined Combined_gear
do
  # Run eval
  Rscript --vanilla --verbose Evaluate-Dungy-Holdout.R $type
done
